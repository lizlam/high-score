class Output

  @@rank = ['1ST', '2ND', '3RD', '4TH', '5TH', '6TH', '7TH', '8TH', '9TH', '10TH']
 
  def html (var)
    content = "<html>\n"
    content << "<title>High Scores</title>\n" 
    content << "<head>\n" 
    content << "<link rel=\"stylesheet\" type=\"text/css\" href=\"highscore.css\">\n"
    content << "</head>\n" 
    content << "<h1>HIGH SCORES</h1>\n"
    content << "<body>\n" 
    content << "<center>\n"
    content << "<table>\n"
    content << "  <tr>\n"
    content << "    <td class=\"col1\"><h1 class=\"body\">RANK</h1></td>\n"
    content << "    <td class=\"col2\"><h1 class=\"body\">SCORE</h1></td>\n" 
    content << "    <td class=\"col3\"><h1 class=\"body\">NAME</h1></td>\n" 
    content << "  </tr>\n"
               var.each_with_index { |v, i| 
                 content << "  <tr>\n"
                 content << "    <td><h1 class=\"body\">#{@@rank[i]}</h1></td>\n"
                 content << "    <td><h1 class=\"body\">#{v[1]}</h1></td>\n"
                 content << "    <td><h1 class=\"body\">#{v[0]}</h1><td>\n  </tr>\n"}
    content << "  <tr><td><h1 class=\"credit\">CREDIT 0</h1></td></tr>\n"
    content << "</table>\n"
    content << "</center>\n"
    content << "</body>\n"
    content <<  "</html>"
    return content
  end 
 
  def user_html (var, branches, labels)
    content =  "<html>\n"
    content <<  "<title>#{var}</title>\n"
    content << head_template 
    content <<  "<h1>#{var}</h1>\n"
    content <<  "<body>\n" 
    content <<  "<center>\n" 
    content <<  "<table>\n"
    content <<  "  <tr>\n"
    content <<  "    <td class=\"status\">\n"
    content <<  "      <h2>Branches\n"
    content <<  "      <meter value=\"#{branches}\" max=\"10\"></meter><br><br>\n"
    content <<  "      </h2>\n"
    content <<  "    </td>\n"
    content <<  "  </tr>\n"
    content <<  "  <tr>\n"
    content <<  "    <td class=\"status\">\n"
    content <<  "      <h2>Labels\n"
    content <<  "      <meter value=\"#{labels}\" max=\"10\"></meter><br><br>\n"
    content <<  "      </h2>\n"
    content <<  "    </td>\n"
    content <<  "  </tr>\n"
    content <<  "</table>\n"
    content << "</center>\n"
    content <<  "</body>\n" 
    content <<  "</html>\n"
    return content
  end

  def head_template
    content =  "<head>"
    content << "<link rel=\"stylesheet\" type=\"text/css\" href=\"highscore.css\">"
    content << "</head>"
    return content
  end

end

