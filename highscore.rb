require_relative 'output'

class HighScore
  @@user_array = Array.new
  @@changes = Hash.new
  @@output = Output.new

  def remove(var, val)
    var.each_with_index do |element, i|
      if element.eql? val 
        var.delete_at(i) 
      end
    end  
  end

  def remove_user(user)
    remove(@@user_array, user)
  end

  def get_users
    puts @@user_array
    puts @@user_array.length
  end

end

class PerforceHighScore < HighScore

  @@jobs = Hash.new
  @@topten = Array.new

  def set_users
    array = Array.new
    result  = %x(p4 -ztag users)
    array = result.encode('UTF-8', :invalid => :replace).split("\n")
    array.each do |line|
        if line.start_with? '... User'
          @@user_array  << line[9, line.length]
        end
    end
  end

  def calc_changes
    array = Array.new
    @@user_array.each do |user|
      result = %x(p4 changes -u #{user})
      array = result.encode('UTF-8', :invalid => :replace).split("\n")
      @@changes[user] =  array.length
    end
    @@topten = @@changes.sort_by { |k, v| v}.reverse.first(10)
    return @@output.html @@topten
  end

  def create_user_profs
    @@topten.each do |user|
      file =  File.new("#{user[0]}.html", "w")
      num_branches = calc_branches user[0]
      num_labels = calc_labels user[0]
      content = @@output.user_html(user[0], num_branches, num_labels)
      file.write(content)
    end
  end

  def calc_branches (user)
    count = calc_spec(user, "branches")
    return array.length 
  end

  def calc_labels (user)
    count = calc_spec(user, "labels")
    return count
  end

  def calc_clients (user)
    count = calc_spec(user, "clients")
    return count
  end

  def calc_spec (user, spec) 
    array = Array.new
    result = %x(p4 #{spec} -u #{user})
    array = result.encode('UTF-8', :invalid => :replace).split("\n")
    return array.length
  end

  def calc_jobs
    array = Array.new
    @@user_array.each do |user|
        result = %x(p4 jobs -e reportedby=#{user})
        array = result.encode('UTF-8', :invalid => :replace).split("\n")
        @@jobs[user] = array.length
      end
      @@output.html @@jobs
  end

end


class GitHighScore < HighScore

  def calc_commits
    array = Array.new
    result = %x(git shortlog -sn)
    array = result.encode('UTF-8', :invalid => :replace).split("\n")
    array.each do |line|
      num = line[0, line.rindex("\t")]
      user = line[line.rindex("\t"), line.length]
      @@changes[user[1, user.length]] = num.lstrip
    end  
    topten = @@changes.sort_by { |h| h[3]}.first(10)
    @@output.html topten    
  end

end

# Git High Score Example
#ghs = GitHighScore.new
#htmlfile = File.new("git.html", "w")
#test = ghs.calc_commits
#htmlfile.write(test)
#htmlfile.close

# Perforce High Score Example
hs = PerforceHighScore.new
hs.set_users
hs.remove_user("git-fusion-user")
htmlfile = File.new("p4.html", "w")
test = hs.calc_changes
htmlfile.write(test)
htmlfile.close
#hs.create_user_profs
